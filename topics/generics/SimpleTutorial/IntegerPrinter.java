package topics.generics.SimpleTutorial;
/**
 * IntegerPrinter
 */
public class IntegerPrinter {

    Integer thingToPring;

    public IntegerPrinter(Integer thingToPrint) {
        this.thingToPring = thingToPrint;
    }

    public void print() {
        System.out.println(this.thingToPring);
    }
}