package topics.generics.SimpleTutorial;

import java.io.Serializable;

/**
 * IntegerPrinter
 */
public class Printer<T extends Animal & Serializable> {

    T thingToPring;

    public Printer(T thingToPrint) {
        this.thingToPring = thingToPrint;
    }

    public void print() {
        System.out.println(this.thingToPring);
        System.out.println( this.thingToPring.eat());
    }
    public void print(T thingToPrint) {
        this.thingToPring = thingToPrint;
        this.print();
    }
}
