package topics.generics.SimpleTutorial;

public class Animal {
    Integer id;

    public Animal() {
        this.id = (int) ((Math.random() * (10000 - 0)) + 0);
    }

    public String eat() {
        return "eating";
    }

    @Override
    public String toString() {
        return "is animal with id: " + this.id;
    }
}
