package topics.generics.SimpleTutorial;

import java.io.Serializable;

public class Cat extends Animal implements Serializable{
    String claws;
    public Cat() {
        super();
        claws = "Garras";
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return "is cat with id: " + this.id;
    }
}
