package topics.generics.SimpleTutorial;

import java.util.ArrayList;
import java.util.List;

/**
 * Main
 */
public class Main {

    public static void main(String[] args) {
        // Printer<Integer> printerInteger = new Printer<>(21);
        // printerInteger.print();

        // Printer<String> printerString = new Printer<>("Olá mundo");
        // printerString.print();

        // Printer<Cat> catPrinter = new Printer<>(new Cat());
        // catPrinter.print();

        // ArrayList<Cat> cats = new ArrayList<>();
        // cats.add(new Cat());
        // Cat myCat = (Cat)cats.get(0);
        // catPrinter.print(myCat);

        shout("john");
        shout(30);
        shout(new Cat());

    }

    private static <T> void shout(T thingToShout) {
        System.out.println(thingToShout + "!!!!!");
    }

    private static <T, V> void shout(T thingToShout, V otherThingToshout) {
        System.out.println(thingToShout + "!!!!!");
        System.out.println(otherThingToshout + "!!!!!");
    }

    private static void printlist(List<? extends Animal> list) {
        System.out.println(list);
    }
}